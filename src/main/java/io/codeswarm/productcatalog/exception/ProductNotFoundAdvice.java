package io.codeswarm.productcatalog.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ProductNotFoundAdvice {

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<ExceptionMessage> noDataHandler(ProductNotFoundException ex) {
        ExceptionMessage em = new ExceptionMessage(LocalDateTime.now(), ex.getMessage());
        return new ResponseEntity<>(em, HttpStatus.NOT_FOUND);
    }
}
