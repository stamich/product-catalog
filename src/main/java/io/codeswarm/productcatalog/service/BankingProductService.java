package io.codeswarm.productcatalog.service;

import io.codeswarm.productcatalog.model.BankingProduct;

import java.util.List;
import java.util.Optional;

public interface BankingProductService {

    List<BankingProduct> findAll();
    List<BankingProduct> findAllByActive(Boolean active);
    Optional<BankingProduct> findById(Long Id);
    BankingProduct create(BankingProduct product);
    void update(Long id, BankingProduct product);
    void deleteById(Long id);
    void deleteAll();
}
