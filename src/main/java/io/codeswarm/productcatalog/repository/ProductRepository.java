package io.codeswarm.productcatalog.repository;

import io.codeswarm.productcatalog.model.Product;
import io.codeswarm.productcatalog.model.ProductDestination;
import io.codeswarm.productcatalog.model.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findAll();
    List<Product> findByCreatedBy(String createdBy);
    List<Product> findAllByActive(Boolean active);
    List<Product> findDistinctByDestinationIn(Set<ProductDestination> productDestinations);
    List<Product> findDistinctByTypeIn(Set<ProductType> productTypes);
    Optional<Product> findById(Long id);
    void deleteById(Long id);
    void deleteAll();
}
