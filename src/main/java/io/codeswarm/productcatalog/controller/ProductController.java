package io.codeswarm.productcatalog.controller;

import io.codeswarm.productcatalog.exception.ProductNotFoundException;
import io.codeswarm.productcatalog.model.Product;
import io.codeswarm.productcatalog.model.ProductDestination;
import io.codeswarm.productcatalog.model.ProductType;
import io.codeswarm.productcatalog.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(path = "/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Product>> getAll(@RequestParam(required = false) String createdBy) {
        List<Product> products = new ArrayList<>();

        if (createdBy == null) {
            products.addAll(productService.findAll());
        } else {
            products.addAll(productService.findByCreatedBy(createdBy));
        }

        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/all/activity")
    public ResponseEntity<List<Product>> getAllByActivity(@RequestParam Boolean active) {
        List<Product> products = productService.findAllByActive(active);

        if (products.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/all/destination")
    public ResponseEntity<List<Product>> getAllByDestination(@RequestParam Set<ProductDestination> productDestinations) {
        List<Product> products = productService.findDistinctByDestinationIn(productDestinations);

        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/all/type")
    public ResponseEntity<List<Product>> getAllByType(@RequestParam Set<ProductType> productTypes) {
        List<Product> products = productService.findDistinctByTypeIn(productTypes);

        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/one/{id}")
    public ResponseEntity<Product> getOneById(@PathVariable("id") Long id) throws ProductNotFoundException {
        Optional<Product> product = productService.findById(id);
        return new ResponseEntity<>(product.get(), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Product> create(@RequestBody Product product) {
        try {
            var entity = productService.create(product);
            return new ResponseEntity<>(entity, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Product> update(@PathVariable("id") Long id,
                                          @RequestBody Product product) throws ProductNotFoundException {
        productService.update(id, product);
        return new ResponseEntity<>(productService.findById(id).get(), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) {
        try {
            productService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/all")
    public ResponseEntity<Void> deleteAll() {
        List<Product> products = productService.findAll();

        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        productService.deleteAll();
        return new ResponseEntity<>(HttpStatus.GONE);
    }
}
