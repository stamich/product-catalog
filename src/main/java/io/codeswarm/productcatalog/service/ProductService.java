package io.codeswarm.productcatalog.service;

import io.codeswarm.productcatalog.exception.ProductNotFoundException;
import io.codeswarm.productcatalog.model.Product;
import io.codeswarm.productcatalog.model.ProductDestination;
import io.codeswarm.productcatalog.model.ProductType;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProductService {

    List<Product> findAll();
    List<Product> findByCreatedBy(String createdBy);
    List<Product> findAllByActive(Boolean active);
    List<Product> findDistinctByDestinationIn(Set<ProductDestination> productDestinations);
    List<Product> findDistinctByTypeIn(Set<ProductType> productTypes);
    Optional<Product> findById(Long id) throws ProductNotFoundException;
    Product create(Product product);
    void update(Long id, Product product) throws ProductNotFoundException;
    void deleteById(Long id);
    void deleteAll();
}
