package io.codeswarm.productcatalog.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import spock.lang.Specification

@SpringBootTest(classes = [ProductControllerSpockTest])
@AutoConfigureMockMvc
@WebMvcTest()
class ProductControllerSpockTest extends Specification {

    @Autowired
    private MockMvc mockMvc

    def "when get is performed then the response has status 200"() {
        expect: "Status os 200"
        mockMvc.perform(MockMvcRequestBuilders.get("/product/all"))
                .andExpect(MockMvcResultMatchers.status().isOk())
    }
}
