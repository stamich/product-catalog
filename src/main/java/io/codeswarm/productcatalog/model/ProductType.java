package io.codeswarm.productcatalog.model;

public enum ProductType {

    MORTGAGE,
    PERSONAL_LOAN,
    STUDENT_LOAN
}
