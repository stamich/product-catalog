package io.codeswarm.productcatalog.model;

public enum ProductDestination {

    CORPORATE,
    INDIVIDUAL
}
