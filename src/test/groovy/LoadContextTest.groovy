import io.codeswarm.productcatalog.controller.ProductController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [LoadContextTest])
class LoadContextTest extends Specification {

    @Autowired(required = false)
    private ProductController productController

//    def "when context is loaded then all expected beans are created"() {
//        expect: "the ProductController is created"
//        productController
//    }

    def "simple assertion" () {
        expect:
        1 == 1
    }

//    def "false assertion" () {
//        expect:
//        1 == 0
//    }
 }
