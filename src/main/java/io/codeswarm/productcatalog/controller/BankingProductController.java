package io.codeswarm.productcatalog.controller;

import io.codeswarm.productcatalog.exception.ProductNotFoundException;
import io.codeswarm.productcatalog.model.BankingProduct;
import io.codeswarm.productcatalog.service.BankingProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/bankingProduct")
public class BankingProductController {

    private final BankingProductService bankingProductService;

    public BankingProductController(BankingProductService bankingProductService) {
        this.bankingProductService = bankingProductService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<BankingProduct>> getAll() {
        List<BankingProduct> products = bankingProductService.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/all/active")
    public ResponseEntity<List<BankingProduct>> getAllByActivity(@RequestParam Boolean active) {
        List<BankingProduct> products = bankingProductService.findAllByActive(active);

        if (products.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/one/{id}")
    public ResponseEntity<BankingProduct> getOneById(@PathVariable("id") Long id) throws ProductNotFoundException {
        Optional<BankingProduct> product = bankingProductService.findById(id);
        return new ResponseEntity<>(product
                .orElseThrow(() -> new ProductNotFoundException(id)), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<BankingProduct> create(@RequestBody BankingProduct product) {
        try {
            var entity = bankingProductService.create(product);
            return new ResponseEntity<>(entity, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<BankingProduct> update(@PathVariable("id") Long id, @RequestBody BankingProduct product) {
        bankingProductService.update(id, product);
        return new ResponseEntity<>(bankingProductService.findById(id).get(), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) {
        try {
            bankingProductService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/all")
    public ResponseEntity<Void> deleteAll() {
        List<BankingProduct> products = bankingProductService.findAll();

        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        bankingProductService.deleteAll();
        return new ResponseEntity<>(HttpStatus.GONE);
    }
}
