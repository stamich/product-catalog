package io.codeswarm.productcatalog.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ExceptionMessage {

    private LocalDateTime timestamp;
    private String message;
}
