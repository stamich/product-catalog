package io.codeswarm.productcatalog.repository;

import io.codeswarm.productcatalog.model.BankingProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BankingProductRepository extends JpaRepository<BankingProduct, Long> {

    List<BankingProduct> findAll();
    List<BankingProduct> findAllByActive(Boolean active);
    Optional<BankingProduct> findById(Long id);
    void deleteById(Long id);
    void deleteAll();
}
