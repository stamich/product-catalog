package io.codeswarm.productcatalog.service;

import io.codeswarm.productcatalog.model.BankingProduct;
import io.codeswarm.productcatalog.repository.BankingProductRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BankingProductServiceImpl implements BankingProductService {

    private final BankingProductRepository bankingProductRepository;

    public BankingProductServiceImpl(BankingProductRepository bankingProductRepository) {
        this.bankingProductRepository = bankingProductRepository;
    }

    @Override
    public List<BankingProduct> findAll() {
        return bankingProductRepository.findAll();
    }

    @Override
    public List<BankingProduct> findAllByActive(Boolean active) {
        return bankingProductRepository.findAllByActive(active);
    }

    @Override
    public Optional<BankingProduct> findById(Long id) {
        return Optional.ofNullable(bankingProductRepository.findById(id))
                .orElse(Optional.of(new BankingProduct()));
    }

    @Override
    public BankingProduct create(BankingProduct product) {
        return bankingProductRepository.save(product);
    }

    @Override
    public void update(Long id, BankingProduct product) {
        Optional<BankingProduct> entity = bankingProductRepository.findById(id);
        entity.get().setUpdatedAt(LocalDateTime.now());
        entity.get().setId(product.getId());
        entity.get().setName(product.getName());
        entity.get().setBankName(product.getBankName());
        entity.get().setExpirationDate(product.getExpirationDate());
        entity.get().setActive(product.getActive());
        bankingProductRepository.save(entity.get());
    }

    @Override
    public void deleteById(Long id) {
        bankingProductRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        bankingProductRepository.deleteAll();
    }
}
